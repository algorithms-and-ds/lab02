﻿#include <windows.h>
#include <stdio.h>
#include <math.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    long long m = ((long long)1) << 32, a = 69069, c = 1, x0;
    const int count = 10000;
    int x[10000];
    int validInput = 0;

    do {
        printf("Введіть початкове значення для генерації псевдовипадкової послідовності x0: ");
        if (scanf_s("%lld", &x0) == 1 && 0 <= x0 && x0 < m) {
            validInput = 1;
        }
        else {
            printf("Помилка! Значення x0 введено неправильно. Відповідь: 0 <= x0 < 2^32\n");
            while (getchar() != '\n'); 
        }
    } while (!validInput);

    for (int i = 0; i < count; i++) {
        x0 = (a * x0 + c) % m;
        x[i] = (double)x0 / m * 300;
    }

    double M = 0, D = 0, Sigma = 0;
    int fr[300];
    double ch[300];

    for (int j = 0; j < 300; j++) {
        int frequency = 0;
        double chance = 0;
        printf("[%3d]: ", j);
        for (int i = 0; i < count; i++) {
            if (x[i] == j)
                frequency++;
        }
        chance = (double)frequency / count;
        fr[j] = frequency;
        ch[j] = chance;
        M += (double)j * chance;
        printf("%d (%lf)\n", frequency, chance);
    }

    for (int i = 0; i < 300; i++) {
        D += pow(i - M, 2) * ch[i];
    }
    Sigma = sqrt(D);

    printf("\n|--------------------------------------|");
    printf("\n| Математичне сподівання      %.2lf   |", M);
    printf("\n| Дисперсія                   %.2lf  |", D);
    printf("\n| Стандартне відхилення       %.2lf    |", Sigma);
    printf("\n|--------------------------------------|");

    return 0;
}

